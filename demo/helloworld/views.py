from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.serializers import ModelSerializer
from .models import SchedulingUnit

# Create your views here.

class SchedulingUnitSerializer(ModelSerializer):
  class Meta:
    model = SchedulingUnit
    fields = '__all__'

class SchedulingUnitViewSet(viewsets.ModelViewSet):
  queryset = SchedulingUnit.objects.all()
  serializer_class = SchedulingUnitSerializer

  @action(methods=['get'], detail=True)
  def trigger(self, request, pk=None):
    SchedulingUnitFlow
    return Response("ok")
