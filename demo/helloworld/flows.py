from django.utils.decorators import method_decorator
from django.db.models.signals import post_save
from viewflow import flow
from viewflow.models import Task
from viewflow.base import this, Flow
from viewflow.flow.views import CreateProcessView, UpdateProcessView, AssignTaskView
from viewflow.activation import FuncActivation, ViewActivation
from viewflow.flow.nodes import Signal
from viewflow import mixins

from .models import HelloWorldProcess, SchedulingUnit

from viewflow import frontend, ThisObject
from viewflow.activation import STATUS

class ConditionActivation(FuncActivation):
    @classmethod
    def activate(cls, flow_task, prev_activation, token):
        activation = super(ConditionActivation, cls).activate(flow_task, prev_activation, token)

        if flow_task.condition_check(activation, None):
            # condition holds on activation
            activation.prepare()
            activation.done()

        return activation

class Condition(Signal):
    #task_type = "HUMAN" # makes it show up in the unassigned task lists
    activation_class = ConditionActivation

    def __init__(self, condition_check, signal, sender=None, task_loader=None, **kwargs):
        """
        Instantiate a Signal task.

        :param signal: A django signal to connect
        :param receiver: Callable[activation, **kwargs]
        :param sender: Optional signal sender
        :param task_loader: Callable[**kwargs] -> Task
        :param allow_skip: If True task_loader can return None if
                           signal could be skipped.

        You can skip a `task_loader` if the signal going to be
        sent with Task instance.
        """
        self.condition_check = condition_check

        super(Condition, self).__init__(signal, self.signal_handler, sender, task_loader, **kwargs)

    @method_decorator(flow.flow_signal)
    def signal_handler(self, activation, sender, instance, **signal_kwargs):
      if activation.get_status() == STATUS.DONE:
          # race condition -- condition was true on activation but we also receive the signal now
          return

      activation.prepare()
      if activation.flow_task.condition_check(activation, instance):
          activation.done()

    def ready(self):
        """Resolve internal `this`-references. and subscribe to the signal."""
        if isinstance(self.condition_check, ThisObject):
            self.condition_check = getattr(self.flow_class.instance, self.condition_check.name)

        super(Condition, self).ready()

@frontend.register
class HelloWorldFlow(Flow):
    process_class = HelloWorldProcess

    # 0. Start on SU instantiation
    # 1. To be Manually scheduled? -> Go to 1a
    #     1a. Present view to manually schedule.
    # 2. Wait on signal SU got finished/error/cancelled (might have already!!) ->
    #                    - Wait for assignment to RO user
    #              View: - Present any quality plots
    #                    - Present any error info
    #                         - Present fixing options
    #                    - Present choice to fix & redo, discard, or continue.
    # Continue:          
    #              View: - Present any quality plots
    #                    - Present any error info
    #                    - Submit quality report/score
    #                    - Submit recommendation
    # 3. - Assign ticket to Contact Author
    #    - Present quality plots to user
    #    - Present quality report/score, and recommendation
    #    - Submit acceptance & report
    # 4. - Assign ticket to owner in step 2.
    #    - Present quality report/score, and recommendation
    #    - Present acceptance & report
    #    - Present choice to ingest or discard.
    # Ingest: 
    #             Set ingestable flag on SU.
    # Discard: - Cancel SU (triggering garbage collection
    #
    # Fix & Redo:
    #    - Wait for user to confirm SU is fixed
    #    - Go to 2
    #             

    # Consider adding to any/all views:
    #    - Present any opened JIRA tickets
    #    - Present opportunity to open JIRA ticket
    # Note that previously submitted info can be found by clicking through the task. So
    # we only need to show whats nominally needed.
    # Note that orthogonally to the above flow:
    #   - Users need to be informed tasks are assigned to them (e-mail?)
    #   - Users already have an overview in viewflow of tickets assigned to them
    #   - We likely want to control what e-mails are sent.

    start = (
        flow.StartSignal(
          post_save,
          this.on_save_can_start,
          sender=SchedulingUnit
        ).Next(this.wait_schedulable)
    )

    wait_schedulable = (
        Condition(
          this.check_condition,
          post_save,
          sender=SchedulingUnit,
          task_loader=this.get_scheduling_unit_task
        )
        .Next(this.form)
    )

    form = (
        flow.View(
            UpdateProcessView,
            fields=["text"]
        ).Permission(
            auto_create=True
        ).Next(this.approve)
    )

    approve = (
        flow.View(
            UpdateProcessView,
            fields=["approved"]
        ).Permission(
            auto_create=True
        ).Next(this.check_approve)
    )

    check_approve = (
        flow.If(lambda activation: activation.process.approved)
        .Then(this.send)
        .Else(this.end)
    )

    send = (
        flow.Handler(
            this.send_hello_world_request
        ).Next(this.end)
    )

    end = flow.End()

    @method_decorator(flow.flow_start_signal)
    def on_save_can_start(self, activation, sender, instance, created, **signal_kwargs):
      if created:
        activation.prepare()
        activation.process.su = instance
        activation.done()
        print("workflow started")
      else:
        print("no workflow started")
      return activation

    def send_hello_world_request(self, activation):
        print(activation.process.text)

    def check_condition(self, activation, instance):
        if instance is None:
            instance = activation.process.su

        condition = instance.state == 5
        print("condition is ",condition)
        return condition

    def get_scheduling_unit_task(self, flow_task, sender, instance, **kwargs):
        print(kwargs)
        process = HelloWorldProcess.objects.get(su=instance)
        return Task.objects.get(process=process,flow_task=flow_task)

