from django.db import models

# Create your models here.

from django.db import models
from viewflow.models import Process

class SchedulingUnit(models.Model):
    name = models.CharField(max_length=50)
    state = models.IntegerField()

class HelloWorldProcess(Process):
    text = models.CharField(max_length=150)
    approved = models.BooleanField(default=False)
    su = models.ForeignKey(SchedulingUnit, blank=True, null=True, on_delete=models.CASCADE)
