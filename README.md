# viewflow-playground

Playing with django-viewflow, extending and abusing the viewflow demo from http://docs.viewflow.io/viewflow_quickstart.html

To setup, run:

python3 -m venv env
source env/bin/activate
pip install django django-material django-viewflow djangorestframework

TO start the server, run:

./run.sh

Note: first time you will have to create a user. That step can be aborted with ^C (or commented out) in subsequent runs.

Go surf to http://127.0.0.1/workflow/ to play with the workflows, and http://127.0.0.1/su/ to create/manage the "scheduling unit" objects.

